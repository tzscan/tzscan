
function change_theme ( theme ) {
  var i=0, links, elt ;
  for (i=0, links = document.getElementsByTagName("link"); i < links.length ; i++) {
    elt = links[i];
    if ((elt.rel.indexOf( "stylesheet" ) != -1) && elt.title) {
      elt.disabled = true ;
      if ((elt.title == ('color-' +  theme)) || (elt.title == ('bootstrap-' + theme))) {
        elt.disabled = false ;
      }
    }
  }
}

function get_cookie ( cookie_name ) {
  var i = 0, cookie;
  var cookies_str = document.cookie;
  if (cookies_str.length != 0) {
    var cookies_array = cookies_str.split( '; ' );
    for (i=0; i < cookies_array.length; i++) {
      cookie = cookies_array[i];
      cookie_value = cookie.match ( cookie_name + '=(.*)' );
      if (cookie_value != null) {
	return decodeURIComponent ( cookie_value[1] ) ;
      }
    }
  }
  return '' ;
}

var current_theme = get_cookie ( 'theme' );
if (current_theme == '')
  change_theme( 'default' );
else
  change_theme( current_theme );
